---
title: About Squidr
linkTitle: About
menu:
  main:
    weight: 10

---


{{< blocks/cover title="About Squidr" image_anchor="bottom" height="min" >}}

<p class="lead mt-5">Making research searchable</p>

{{< /blocks/cover >}}







{{% blocks/lead %}}



Welcome to Squidr


<span style="text-align:left;">


The Squidr data repository is a secure research infrastructure for storing, sharing and (re)use of data from intervention, epidemiological, in vitro and animal studies. Squidr is compatible with GDPR legislation and supports sharing according to the <a href="https://www.go-fair.org/fair-principles/">FAIR principles</a>.


In the Squidr database your data will be uniformly structured which promotes easy reuse and allows searching across studies as well as granting Fine Grained Access Control (FGAC) to your data.


The code is freely available from <a href="https://gitlab.com/nexs-metabolomics/Squidr-core">GitLab</a> under a BSD 3-Clause License, and installable on servers anywhere. Privately controlled servers can participate in a federated network allowing you to keep control of your institutions data while making your data FAIR. 

<ul style="display: inline-block;text-align: left;">
<li>Purpose and vision</li>
<li>Detailed information</li>
<li>List of words and standards</li>
<li>Whodunnit</li>
</ul>

</p>
</span>

{{% /blocks/lead %}}





{{< blocks/section >}}
<div class="col-12">
	<h1 class="text-center">Purpose and vision</h1>

	<p>
	The overall purpose of Squidr is to make it easy for researchers worldwide to share data in a <a href="https://www.go-fair.org/fair-principles/">FAIR manner</a> while respecting ethical and GDPR boundaries.
	</p>
	
	<p>
	The vision is that all universities and research institutions establish Squidr servers to curate and share their own data and that the full federated network of Squidr nodes can be queried to find and retrieve existing data to support novel research. Access to the data can be easily controlled as needed, though we encourage as much openness as possible.
	</p>
	
	<p>
	Existing databases for research data sharing have hitherto fallen into two main categories:
	
		<ol>
			<li>Domain specific databases, e.g. for genomic data and</li>
			<li>General purpose database that constitute more unstructured “data dumps”.</li>

		</ol>  
	</p>
	
	<p>
	The main innovation in Squidr is that all data are structured and strictly linked to the study design such that the data is easily understood by other researchers and can be used directly for further analyses and searching across studies. In addition tabular data of all kinds can be uploaded.
	</p>

</div>

{{< /blocks/section >}}









{{< blocks/section >}}

<div class="col-12">
<h1 class="text-center">This is another section</h1>
[Some text](linky)
</div>

{{< /blocks/section >}}







{{< blocks/section >}}

<div class="col-12">
<h1 class="text-center">This is another section</h1>
</div>

{{< /blocks/section >}}
