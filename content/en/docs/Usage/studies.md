---
title: "Studies"
linkTitle: "Adding studies"
weight: 2
description: >
  How to add studies to the system
---

{{% pageinfo %}}
WIP
{{% /pageinfo %}}



## Create study

**Create the study**

* "Studies" --> "Manage Study" --> "Add/Edit" --> "Create new"
* Fill study short name, title and description
* → "OK"

**Description**

* You are moved to the "Edit" and under "General" you can fill out the general details of the study description.
* Under "Design" you can detail the description of the study design



## Import design to the study description

The study design needs to be described in  a structured way. The easiest way to start out is to import the design from the dataset and amend as appropriate.

* Studies --> Study design --> "Import data design"



*Note: If the first dataset you intend to upload does not contain all combinations of design parameters the complete study contains it is a good idea to construct a dummy dataset that contains all combinations of design parameters. This makes it easier to import additional datasets that have more design factor combinations than the first dataset.*



## Add dataset

How we can add the uploaded dataset to the study.

* "Studies" --> <span style="background-color: yellow">"Datasets"</span> --> "Add dataset" --> select the study and the dataset you want to add data to
* Now confirm that the design is correct.
* click "add dataset" --> "confirm"
* If the dataset does not match the previously uploaded study design, there might be an error in compatibility between the dummy dataset and the actual datasets. Check which design parameters do not match and make appropriate edits in the .csv file.
* "Summary" --> "Datasets" --> to see datasets matched to study. 





## Refine study design description

