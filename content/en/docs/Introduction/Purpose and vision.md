---
title: "Purpose and vision"
linkTitle: "Purpose and vision"
weight: 1
description: >
  Purpose and vision of Squidr
---

The overall purpose of Squidr is to make it easy for researchers worldwide to share structured (final) data in a [FAIR manner](https://www.go-fair.org/fair-principles/) while respecting ethical and personal data safety boundaries. 

Sharing can be open, partial or closed for each dataset and visibility may target only certain researchers/students, or anyone. Links to raw data may also be added. 

Within a project group, Squidr may serve as a hub for structured data. Later, e.g. after publishing, the data may be shared more broadly as required.

Squidr has been designed with nutrition research in mind but should be quite open for any experimental or observational study within biology, and possibly even broader.

The vision is that all universities and research institutions could establish Squidr servers to curate and share their own data and that a full federated network of Squidr nodes in future updates could be queried to find and retrieve existing data supporting novel research. 

Most of the existing databases for sharing research data have hitherto fallen into two main categories:

1. Domain specific databases, e.g. for genomic data or
2. General purpose databases for raw data that constitute “data dumps”.

The main innovation in Squidr is that all data are structured and strictly linked to the study design. This means that the data is easily understood by other researchers and can be used directly for further analyses and even queried across studies. The database contains searchable design meta-data along with *tabular* data representing outcomes of all kinds.

