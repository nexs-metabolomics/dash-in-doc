---
title: "Introduction"
linkTitle: "Introduction"
weight: 2
description: >
  What is Squidr?
---

Welcome to Squidr!<br/>

The Squidr data repository is a secure research infrastructure for storing, sharing and (re)use of data from intervention, epidemiological, *in vitro* and animal studies. Squidr includes study design variables, curated outcomes and even annotated ‘omics data. It is compatible with GDPR legislation and supports sharing according to the [FAIR principles](https://www.go-fair.org/fair-principles/).

In the Squidr database your data will be uniformly structured facilitating easy reuse and allowing users to search across studies as well as granting Fine Grained Access Control (FGAC) to others of their shared data.

The code is freely available from [GitLab](https://gitlab.com/nexs-metabolomics/dash-in-core) under a BSD 3-Clause License, and installable on servers anywhere. Servers can participate in a federated network allowing institutions and researchers to keep control of their data while making them FAIR.
