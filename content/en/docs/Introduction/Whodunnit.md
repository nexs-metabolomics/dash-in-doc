---
title: "Whodunnit"
linkTitle: "Whodunnit"
weight: 4
---

The Squidr system has been developed by Finn Sandø-Pedersen (programming), [Jan Stanstrup](https://research.ku.dk/search/result/?pure=en%2Fpersons%2Fjan-stanstrup(37da35f5-5398-4e80-a1ce-67365dd36317).html) (testing, curation, documentation), [Susanne Riber](https://susanneriber.dk/) (graphical design), and [Lars Ove Dragsted](https://nexs.ku.dk/english/staff/?pure=en/persons/52173) (ideation and system outline). 

The Squidr system builds upon work performed under [NuGO](http://www.nugo.org/) and is aligned with the existing data repository system, dbNP, the [Nutrition Phenotype database](https://www.dbnp.org/), developed by [Jildau Bouwman](https://www.tno.nl/en/collaboration/find-a-tno-employee/jildau-bouwman/cid22505/), TNO, The Netherlands. In principle, the two systems can communicate and share data, however there are several structural differences.

Over time work it has been supported by the project, ENPADASI under the Joint Programming Initiative, [“A Healthy Diet for a Healthy Life”](https://www.healthydietforhealthylife.eu/index.php/enpadasi), through the Danish Innovation Foundation and continued by a Semper Ardens Grant from the Carlsberg Foundation, and the PRIMA project (https://www.prima-food.eu/) grant from the Novo-Nordisk Foundation, all in part to Lars O. Dragsted. 

