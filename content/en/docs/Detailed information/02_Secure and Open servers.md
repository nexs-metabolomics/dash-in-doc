---
title: "Secure and open servers"
linkTitle: "Secure and open servers"
weight: 2
---

Squidr may be installed freely as an internal system behind a firewall to collect and store curated data safely within an institution; freely available data can be shared from a separate installation on an open server at the same institution at any University or research institution offering an open server with a DASH-IN installation. An interface is under development for DASH-IN open servers to communicate in order to allow searching and sharing across all installations.

