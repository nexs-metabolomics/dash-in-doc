---
title: "Vocabulary and concept"
linkTitle: "Vocabulary and concept"
weight: 1
---

A detailed explanation of the terms used in Squidr can be found in the *Words and Standards* subsection.

To enter your data from a study into Squidr, you start by uploading a core structured *dataset* from the study that you wish to *store* or *share*. This will help you to describe the *study design*. There are often several datasets in a study, e.g. anthropometry, clinical chemistry, 'omics measurements, etc. 
The best dataset to select initially for upload into Squidr represents the *study design* well, i.e. there is a data value for each subject and time point in the study; for instance if the volunteers were weighed at each visit then the dataset containing all the weight measurements and other anthropometry data would span the study design. This will help you to provide the *study design* directly in the study record. Some general information on the study (*study metadata*) together with the *study design* forms a *study record* (in the database this is just called a ”*study*”).

Additional *datasets* can be added to a study at any time. The *datasets* consist of *variables* with their *values* for each study object (e.g. participant, animal, petri dish) or derived *sample* (e.g. a urine sample) measured by an *assay*. 
These *values* will often have [*units*](/docs/faq/units), but *variables* can also have associated Variable Metadata, e.g. a method description, mass fragments or digital spectral data. 
Variables may also exist as Arrays or Matrices for each sample, as they would typically do after any kind of ‘omics' analysis, e.g. metabolomics. For each entry in a metabolomics dataset, metadata would typically be the compound identity, the level of confidence, mass spectral data, etc. A step-by-step guide to upload your data is found under “usage”.





