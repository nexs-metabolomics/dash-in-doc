---
title: "List of words and standards"
linkTitle: "List of words and standards"
weight: 100
---

The following alphabetical list of terms and explanation of standards used in Squidr is work in progress and will be updated from time to time:

ARRAY VARIABLES: These are variables consisting of a data array, e.g. a set of answers to a structured questionnaire.

ASSAY: An analytical procedure, typically conducted in a laboratory, that will result in a defined *measurement* on a *sample*.

CENTER: This *design parameter* indicates the research site where the *event*, e.g. a visit in a human *trial*, is taking place.

COLUMN: The vertical direction of the data in a dataset; the column headers are describing their content.

DATASET: A set of data organized in a *tidy* table. Each piece of information (datapoint) will exist in a cell, defined by a column and a row having headers to explain the particular datapoint.

DATA UPLOAD: Entering data into Squidr; most of the data will be entered by uploading *datasets* using the upload button in the dataset menu.

DESIGN PARAMETERS: The design parameters or design variables are those fixed variables in Squidr that will define any datapoint in a dataset. They include *ID* (a study object or subject), *Event* (the sequence number of visits or other main actions comprising the study), *Subevent* (e.g. a treatment of the study objects/subjects), *sammpling event* (event with sample collection), *sampling ordinal time* (the actual sequence or time of samplings during a sampling event), *Startgroup* (study arm), and *Center* (location).

EFFECT: 

EVENT: Another *design parameter* providing a time line. Typically this could be the volunteer visits (in a human study with visits 1, 2, 3 etc.) or the sequential shifts of cell culture flasks; the events comprise any sequence of times at which study objects/subjects were handled somehow, thereby providing the overall time line of the study. There might not necessarily be more than a single EVENT, e.g. in a cross-sectional (*observational*) study design, and in that case the EVENT can be left unassigned.

EXPERIMENTAL STUDY: A type of scientific investigation, where the scientist(s) manipulate study objects or subjects in order to fully control a certain exposure. There are many experimental *study designs*, typically with several treatment groups, and the best quality designs have a control or placebo treatment group in order to assure that any outcome in other treated groups differ from outcomes in the placebo group.

EXPOSURE: Any environmental variable impacting on an individual. This may be diet/foods, medication, physical exercise, psychological stressors, or environmental pollutants from air, water or food.

FAIR: 

LABEL: An additional name or tag that explains a variable, object/subject, *assay* or other entities in Squidr. A set of labels and associated information can be uploaded into squidr as *variable info* to assist in the explanation of a study.

MATRIX VARIABLES: an n-dimensional variable such as a metabolomics or otrher 'omics profile from a sample can be termed, matrix variables.

OBSERVATIONAL STUDY: Any study design, where the conducting scientist is only an observer and does not interfere with the *exposure* of subjects/objects. The observations may include actual measurements on subjects or samples collected in the study. There are a number of observational study designs, e.g. cross-sectional (all observations of exposures and effects measured at a single time point), retrospective (recording past exposures for subjects with/without a specific outcome such as a disease), and prospective (cohort) designs where information is collected initially and the subjects are followed up later after several years, often using public or hospital reegisters to identify outcomes (typically diagnoses).  

OPEN ACCESS:

PI: 

RIGHTS: 

ROW: The horizontal direction of the data in a dataset; the row headers are describing their content.

SAMPLE: A sample most often refers to a biological subsection isolated from the study subject or object, e.g. a urine sample, a blood sample, or the serum or plasma derived from blood. Measurements using biological *assays* are often performed on such samples to gain information on exposures, effects or susceptibilities of the subject. However, the whole study subject may also be a "sample", e.g. when a volunteers is weighed or bioimpedance is measured.

SAMPLING EVENT: This *design parameter* is used for indicating biological, questionnaire, or other samplings. The type of sampling that took place must indicate the sample types used for *variable* measurement , e.g. plasma for glucose, serum for lipids, urine for creatinine, or even the entire subject for measurements of height, age, questions, etc. In addition, sub-samplings, such as isolated proteins or DNA from blood represent separate sampling events because they result in unique samples, used for measurement of *variables*. 

SAMPLING ORDINAL TIME: Sequential sampling during a single SAMPLING EVENT can be indicated here. For example, a sequence of three plasma samples collected after a challenge test (SUBEVENT associated with a SAMPLING EVENT) may be assigned as 1, 2, 3 if blood was collected, centrifuged and plasma measured three times during a SAMPLING EVENT. Such a SAMPLING EVENT is typically taking place at the end of a SUBEVENT with a specific treatment, e.g. a meal or drink. Ordinal time is used because any relation to real clock time may not be observed and precise clock time might differ slightly for each observation. If real clock time was recorded this can be provided as a *label*.

SHARING: Opening access to specific datasets or studies to other scientists; these scientists may be from your department, from your project group, a group of your students, or the entire world. Most often data are initially shared within a project group or consortium running a series of studies, and therefore sharing can be done within an *organization* in Squidr, so that only those having the password to the particular organization will be able to see or access the data, depending on the *rights* they have. Rights are provided by the *PI*. Sharing openly is an ideal in science since it strongly supports scientific development. The standard for open sharing is *FAIR* (making data Findable, Accessible, Interoperable, and Reusable).

SQUIDR: 

STARTGROUP: The major *design parameter*, and typically a “study arm” or “randomization group”. This is the group of “*subjects*” that receive the same treatment(s) in the same order. In a cross-over study each randomization group in the study should be treated as a separate STARTGROUP because they receive the (same) treatments in different orders. In observational studies, there is only one STARTGROUP since all subjects are untreated.

STORING:

STUDY:

STUDY DATABASE RECORD

STUDY DESIGN:

SUBEVENT: This is the next level of *design parameters* and typically represents what was done to the subjects during each EVENT. Typically, the main intervention is indicated here (e.g. control vs. treatment with verum). There might be a separate sequence of sub-events for each STARTGROUP, e.g. in a simple cross-over trial the SUBEVENTs would be 1) treatment A, 2) treatment B for the first STARTGROUP but for the second STARTGROUP it would be 1) treatment B, 2) treatment A. There may be several sub-events in complex studies, e.g. dietary changes as well as physical or dietary challenges.

SUBJECT: 

SUBJECT_ID: The “who/what” under observation. Typically, a person, animal or flask being observed or exposed in the study. Note: remember to not include information here that could identify a person (e.g. birthdate, social security number, etc.) – keep DASH-IN anonymized or at least pseudo-anonymized (if you still keep identities of human subjects then keep in a separate, secured place of storage!).

SUSCEPTIBILITY: 

TIDY FORMATS: Tidy formats or long formats are data tables where each unique data point has its own row. The solumns in the table consist of the design variables and the different kinds of measurement variables coming from direct measurements on the subjects or from assays performed on the samples.

TIME: Experimental and observational studies have designs defined as *events*, happening in sequence, e.g. a *baseline* visit followed by a certain number of later contacts or visits, often V1, V2, ..., where the sequence of these visits is the TIME variable, a simple positive number for the visit/contact number in the series. 

TREATMENT: A treatment is a term used in *experimental studies* to describe an intervention inflicted on the subjects by the experimenter. This could be a special food, diet, medication, or exercise taking place at a defined time (*event*) in the experiment. Each separate kind of treatment within an experiment is termed a *subevent*. In *observational studies* there are usually no treatments, however certain tests such as a glucose tolerance test taking place at baseline or later would also constitute a subevent.

TRIAL:

UNITS: All units in Squidr use the international standard for units (SI) but other units are allowed and may be translated. Please see the special section on units.

VALUE:

VARIABLE(S):

VARIABLE METADATA:

 
