---
title: "Detailed information"
linkTitle: "Detailed information"
weight: 3
---

Scientific data should be stored safely not only as raw data but also in a curated, standardized format which is accessible and reusable for future research. This is the basis of the [FAIR principles](https://www.go-fair.org/fair-principles/). However, doing this in practice is difficult for most researchers because data repositories for doing this are few and provide limited flexibility and capabilities. 

With Squidr we have tried to produce a truly versatile solution for curated data from any study design. A vocabulary has been developed that should be quick to learn and applicable to all studies, thereby allowing a common data structure and full querying capabilities for all experimental studies. 

With time this will be aligned with related ontologies. The current solution was developed for nutrition and health studies but should fit other purposes within biology, medical sciences, and sociology. Please provide feedback if you lack flexibility in other study areas.

