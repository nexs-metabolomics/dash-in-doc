---
title: "Data curation before entry"
linkTitle: "Data curation before entry"
weight: 3
---

## What should the data look like before uploading?

The only strict requirement for the data, in order to be be added to the Squidr database is that data are organized in a tabular *tidy* format (also sometimes called “long format” or “vertical format”), see Figure 1. Being tidy data means that each of the *design parameters*, *subject* (ID), *time* (Event), *treatment* (Subevent), *event with sample collection* (sampling event), *the actual time of sampling* (sampling time), *study arm* (Startgroup), and *Center* (location, in case the study is performed at several sites)) and each of the measured *variables* are placed each in only one *column* and that each *observational entity* is placed in a single *row*. The *study object* should be understood as the *subjects* investigated in the study (typically a person, animal, cell culture flask or other material under observation) as a function of the *design parameters*. 

NB! If your data has *design parameters* (e.g. the day or the treatment) included as a column heading, then the data sheet is *NOT* tidy! So a heading such as “glucose_visit5” has no place in tidy data.

<br/>

![upload_ready_data_smaller](../../../figures/upload_ready_data_smaller.png)
**Figure 1.** Example data structure for a crossover study with two treatments (A/B), 50 subjects and 3 time points. In this example there is only one *sampling type* and one *sampling time* (nested under *sampling type*).

<br/>

Therefore, the same *subject* will occupy as many rows as needed to satisfy a unique combination of all *design parameters*. If for example a subject was measured on three different days during each of two different treatments, there would be 3\*2=6 rows for each subject. The observations for each of the six separate rows would be guided by columns indicating the value of each *design parameter*, i.e. which subject, time and treatment the observations belongs to. In the data table, you thus have *design parameters as column headings. In addition, each of the measured variables would have one column. Let’s say we measure* glucose and insulin *we thus have two* measurement columns.

The total number of rows in the dataset is the number of *design parameter* combinations times the number of *subjects* given that all combinations of *design parameters* have actually been recorded for all *subjects*. So, in the example above for a study with 50 participants, there would be 50*6=300 rows (again, given that all measurements were done and e.g. no samples missing). In general missing values are just left empty or filled with *ND* or *NA* within the row.

In case different batches of samples are present in your dataset, the dataset may then be split into several datasets, each containing information on only one batch. Datasets must then be uploaded separately and can be merged into one study afterwards. 

We impose this uniform table structure in Squidr to ensure that the database can work for any type of tabular data with just one common data structure; that data structure allows easy data reuse and facilitates querying across studies. This structure will also flow right into most modern statistical analysis tools.

We do not impose any restrictions on your (*tidy*) column names but instead (after uploading the data) you are required to explicitly indicate (map) which column contains which design variables that Squidr can utilize to structure the data.

We have defined 6 generic *design parameters* which we believe are able to describe (almost) any study and we use the following terms to describe them (you can find additional terms in the *words and standards* section):

1. SUBJECT_ID: The “who/what” under observation. Typically, a person, animal or flask being observed or exposed in the study. Note: remember to not include information here that could identify a person (e.g. birthdate, social security number, etc.) – keep DASH-IN anonymized or at least pseudo-anonymized (if you still keep identities of human subjects then keep in a separate, secured place of storage!).

2. STARTGROUP: The major *design parameter*, and typically a “study arm” or “randomization group”. This is the group of “*subjects*” that receive the same treatment(s) in the same order. In a cross-over study each randomization group in the study should be treated as a separate STARTGROUP because they receive the (same) treatments in different orders. In observational studies, there is only one STARTGROUP since all subjects are untreated.

3. EVENT: Another *design parameter* providing a time line. Typically this could be the volunteer visits (in a human study with visits 1, 2, 3 etc.) or the days where cell culture flasks were shifted, so any sequence of times at which subjects were handled somehow, thereby providing the overall time line of the study. There might not necessarily be more than a single EVENT, e.g. in a cross-sectional study design, and in that case the EVENT can be left unassigned.

4. SUBEVENT: This is the next level of *design parameters* and typically represents what was done to the subjects during each EVENT. Typically, the main intervention is indicated here (e.g. control vs. treatment with verum). There might be a separate sequence of sub-events for each STARTGROUP, e.g. in a simple cross-over trial the SUBEVENTs would be 1) treatment A, 2) treatment B for the first STARTGROUP but for the second STARTGROUP it would be 1) treatment B, 2) treatment A. There may be several sub-events in complex studies, e.g. dietary changes as well as physical or dietary challenges.

5. SAMPLING EVENT: This *design parameter* is used for indicating biological, questionnaire, or other samplings. The type of sampling that took place must indicate the sample types used for *variable* measurement , e.g. plasma for glucose, serum for lipids, urine for creatinine, or even the entire subject for measurements of height, age, questions, etc. In addition, sub-samplings, such as isolated proteins or DNA from blood represent separate sampling events because they result in unique samples, used for measurement of *variables*. 

6. SAMPLING ORDINAL TIME: Sequential sampling during a single SAMPLING EVENT can be indicated here. For example, a sequence of three plasma samples collected after a challenge test (SUBEVENT associated with a SAMPLING EVENT) may be assigned as 1, 2, 3 if blood was collected, centrifuged and plasma measured three times during a SAMPLING EVENT. Such a SAMPLING EVENT is typically taking place at the end of a SUBEVENT with a specific treatment, e.g. a meal or drink. Ordinal time is used because any relation to real clock time may not be observed and precise clock time might differ slightly for each observation. If real clock time was recorded this can be provided as a *label*.

7. CENTER: This design parameter indicates the research site where the event, e.g., visit in a human trial, is taking place. 


