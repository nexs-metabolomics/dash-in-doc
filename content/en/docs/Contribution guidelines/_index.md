---
title: "Contribution to Squidr development"
linkTitle: "Development contribution"
weight: 10
description: >
  How to contribute to the docs
---

{{% pageinfo %}}
How to contribute to the project
{{% /pageinfo %}}

All submissions, including submissions by project members, require review. We use GitLab pull requests for this purpose. Consult [GitHub Help](https://help.github.com/articles/about-pull-requests/) for more information on using pull requests.



## Documentation website

### Updating a single page

We use [Hugo](https://gohugo.io/) to format and generate our documentation website and the [Docsy](https://github.com/google/docsy) theme for styling and site structure.
You write the pages in Markdown (or HTML), and Hugo wraps them up into a website.

If you've just spotted something you'd like to change while using the docs, we has a shortcut for you:

1. Click **Edit this page** in the top right hand corner of the page.
1. If you don't already have an up to date fork of the project repo, you are prompted to get one - click **Fork this repository and propose changes** or **Update your Fork** to get an up to date version of the project to edit. The appropriate page in your fork is displayed in edit mode.

### Previewing your changes locally

If you want to run your own local Hugo server to preview your changes as you work:

1. Follow the instructions in [Getting started](/docs/getting-started) to install Hugo and any other tools you need. You'll need at least **Hugo version 0.45** (we recommend using the most recent available version), and it must be the **extended** version, which supports SCSS.
1. Fork the [Squidr Doc repo](https://gitlab.com/nexs-metabolomics/dash-in-doc) repo into your own project, then create a local copy using `git clone`. Don’t forget to use `--recurse-submodules` or you won’t pull down some of the code you need to generate a working site.

    ```bash
    git clone --recurse-submodules --depth 1 https://gitlab.com/nexs-metabolomics/dash-in-doc.git
    ```

1. Run `hugo server` in the site root directory. By default your site will be available at http://localhost:1313/. Now that you're serving your site locally, Hugo will watch for changes to the content and automatically refresh your site.
1. Continue with the usual GitHub workflow to edit files, commit them, push the
  changes up to your fork, and create a pull request.

### Creating an issue

If you've found a problem in the docs, but you're not sure how to fix it yourself, please create an issue in the [Squidr Doc repo](https://gitlab.com/nexs-metabolomics/dash-in-doc). You can also create an issue about a specific page by clicking the **Create Issue** button in the top right hand corner of the page.

### Useful resources

* [Docsy user guide](wherever it goes): All about Docsy, including how it manages navigation, look and feel, and multi-language support.
* [Hugo documentation](https://gohugo.io/documentation/): Comprehensive reference for Hugo.
* [Github Hello World!](https://guides.github.com/activities/hello-world/): A basic introduction to GitHub concepts and workflow.



## Squidr core

Squidr core is the software that runs the database and website where you find and upload the data. We welcome contributions in general and would be happy to collaborate on new features.

To simply open and issue to make suggestions or report a but head over the the [repo's issues](https://gitlab.com/nexs-metabolomics/dash-in-core/-/issues) and create an issue.

To contribute to the project you should
* fork the [repository](https://gitlab.com/nexs-metabolomics/dash-in-core)
* Make your changes locally
* Push the changes to your fork
* Make a pull request