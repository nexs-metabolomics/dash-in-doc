---
title: "Units"
linkTitle: "Units"
weight: 8
date: 2020-11-04
description: >
  Learn how to work with the units of your measurements
---

{{% pageinfo %}}
Work in progress

{{% /pageinfo %}}



## What is units?

When you report the measurement of something the "unit of measurement" should also be reported. If for example you measure the height of a person it does not make sense to report just "150"; you need to specify that the unit is in centimeters. Otherwise you cannot be sure that measurements can be compared.
For the purpose of comparing and searching different datasets Squidr is able to automatically convert the measurements' units to the unit specified by the user.

**Data-wise that means that for each measured variable in your dataset you need to define the appropriate unit.**

</br>



## How are units defined in Squidr?

In Squidr the unit is always principally described in two parts: X per Y. For example a concentration could have been measured in grams per liter (g/L). You selected grams and liter independently.

In some situations the second part of the definition is not relevant. For example if you report the amount of urine produced by a subject. In that case the second part would be unitary 1 (a formal SI unit, the [Uno](https://en.wikipedia.org/wiki/Parts-per_notation#Uno), has been proposed but not adopted). In such cases the second part should be set to "" (blank). **It is important to know that if any of the two parts are left as "N/A" then the whole unit is considered as  <u>un</u>defined.**

### Unitless measurements

In some situations the measurement is truly unitless/[dimensionless](https://en.wikipedia.org/wiki/Dimensionless_quantity). Examples include fractions such as [mass fractions](https://en.wikipedia.org/wiki/Mass_fraction_(chemistry)) or [mole fractions](https://en.wikipedia.org/wiki/Mole_fraction) but also units like pH.

</br>



## Adding new units to Squidr



</br>