---
title: "Security (technical and GDPR)"
linkTitle: "Security "
weight: 5
description: >
  How we safeguard the data
---

{{% pageinfo %}}
How to contribute to the project
{{% /pageinfo %}}

Secure data sharing (System architecture, FGAC, Data Shield, Ethics and data safety)
## Squidr is only for anonymous data
The Squidr system is NOT designed to contain person-identifiable information. It is therefore born as GDPR-compliant, and any study participant will only be known to the system with a unique system-generated identifier. In case a subject participates in two distinct studies, this information will not be known to the system and the participant will be represented by two independent identifiers.

## Ethics and responsibility of the PI
The study PI will be responsible for uploading only information with ethical permission for collection and sharing among eligible researchers. When a study is fully anonymized (all person-identifiable physical or online records have been destroyed) many countries accept open and FAIR sharing. It is the responsibility of the PI to secure that it is legal to share and specific data. The developers will have no liability regarding uploads of data to systems where they are not system administrators.

## Dividing data by organizations to keep them confined to eligible researchers
Study data and their datasets can be uploaded to a specific organization (e.g. a project group), dwhich is created by a system administrator on the request of the PI responsible for the organization. Only named subjects will have password access to  any data within an organization. The administrator of an organization can define new users and provide passwords and roles.
Datasets can be publicly shared by ticking and confirming open sharing, which means they will be visible outside of the organization.

## Datasets
Study records in Squidr consist of a collection of datasets, glued together by a unique identifier. This identifier is generated when the first dataset is uploaded along with the design variables that define the study and each unique participant or sample. The first dataset in a study therefore ideally contains a variable for each participant, study group, visit, sampling event, time point, sample type, and study center. But adding datasets with additional unique combinations of these design variables is possible at any time, e.g. adding a new center or time point. 

## Variables
Current access control is only available at the dataset level. To restrict access to certain variables manually split your dataset appropiately and assign access to the restricted set of variables you wish to make available.

